﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace FileUploadApp.Migrations
{
    public partial class uploadfilesmodeladded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UploadedFileses",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    FileName = table.Column<string>(nullable: true),
                    PreviewId = table.Column<string>(nullable: true),
                    OrganizationStageId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UploadedFileses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UploadedFileses_OrganizationsStaging_OrganizationStageId",
                        column: x => x.OrganizationStageId,
                        principalTable: "OrganizationsStaging",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UploadedFileses_OrganizationStageId",
                table: "UploadedFileses",
                column: "OrganizationStageId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UploadedFileses");
        }
    }
}

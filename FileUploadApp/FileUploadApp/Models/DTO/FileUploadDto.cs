﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileUploadApp.Models.DTO
{
    public class FileUploadDto
    {
        public FileUploadDto()
        {
            OrganizationStages = new List<OrganizationStage>();
        }
        public string FileName { get; set; }
        public string PreviewId { get; set; }
        public List<OrganizationStage> OrganizationStages { get; set; }
    }
}

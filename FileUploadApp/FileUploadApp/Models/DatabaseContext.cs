﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace FileUploadApp.Models
{
    public class DatabaseContext:DbContext
    {
        public DatabaseContext(DbContextOptions options):base(options)
        {
            
        }
        public DbSet<OrganizationStage> OrganizationsStaging { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<UploadedFiles> UploadedFileses { get; set; }
    }
}

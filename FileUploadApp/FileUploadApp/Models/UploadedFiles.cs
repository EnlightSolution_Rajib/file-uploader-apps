﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileUploadApp.Models
{
    public class UploadedFiles
    {
        public long Id { get; set; }
        public string FileName { get; set; }
        public string PreviewId { get; set; }
        public OrganizationStage OrganizationStage { get; set; }
    }
}

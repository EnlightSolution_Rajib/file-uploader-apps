﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileUploadApp.Models
{
    public class Organization
    {
        public long Id { get; set; }
        public string OrganizationName { get; set; }
        public string Address { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FileUploadApp.Models
{
    public class OrganizationStage
    {
        public long Id { get; set; }
        public string OrganizationName { get; set; }
        public string Address { get; set; }
        public string PreviewId { get; set; }
    }
}

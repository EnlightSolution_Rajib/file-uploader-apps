﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileUploadApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FileUploadApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UploadFilesController : ControllerBase
    {
        private DatabaseContext _db;
        public UploadFilesController(DatabaseContext db)
        {
            _db = db;
        }
        // GET: api/UploadFiles
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var uploadedFiles = await _db.UploadedFileses.ToListAsync();
            if (uploadedFiles.Any())
            {
                return Ok(uploadedFiles);
            }

            return BadRequest("No file found!");
        }

        // GET: api/UploadFiles/5
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST: api/UploadFiles
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/UploadFiles/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

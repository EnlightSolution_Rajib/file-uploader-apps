﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileUploadApp.Models;
using FileUploadApp.Models.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FileUploadApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrganizationsStagingController : ControllerBase
    {
        private DatabaseContext _db;
        public OrganizationsStagingController(DatabaseContext db)
        {
            _db = db;
        }
        // GET: api/OrganizationsStaging
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET: api/OrganizationsStaging/5
        [HttpGet("{previewId}")]
        public async Task<IActionResult> Get(string previewId)
        {
            var organizationStagingItems = await _db.OrganizationsStaging.Where(c => c.PreviewId == previewId).ToListAsync();
            
            if (organizationStagingItems.Any())
            {
                return Ok(organizationStagingItems);
            }

            return BadRequest("No organizations found!");
        }

        // POST: api/OrganizationsStaging
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] FileUploadDto uploadFiles)
        {
            List<OrganizationStage> organizationStages = new List<OrganizationStage>();
            organizationStages = uploadFiles.OrganizationStages;
            UploadedFiles uploaded = new UploadedFiles();
            uploaded.FileName = uploadFiles.FileName;
            uploaded.PreviewId = uploadFiles.PreviewId;

            await _db.OrganizationsStaging.AddRangeAsync(organizationStages);
            if (_db.SaveChanges() > 0)
            {
                await _db.UploadedFileses.AddAsync(uploaded);
                if (_db.SaveChanges() > 0)
                {
                    return Ok("Saved");
                }
            }

            return BadRequest("Operation failed!");
        }

        // PUT: api/OrganizationsStaging/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

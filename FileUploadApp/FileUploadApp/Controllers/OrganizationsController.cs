﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FileUploadApp.Models;
using FileUploadApp.Models.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FileUploadApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrganizationsController : ControllerBase
    {
        private DatabaseContext _db;
        public OrganizationsController(DatabaseContext db)
        {
            _db = db;
        }
        // GET: api/Organizations
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Organizations/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Organizations
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]FileUploadDto uploadFiles)
        {
            List<OrganizationStage> organizationStages = new List<OrganizationStage>();
            organizationStages = uploadFiles.OrganizationStages;
            UploadedFiles uploaded = new UploadedFiles();
            uploaded.FileName = uploadFiles.FileName;
            uploaded.PreviewId = uploadFiles.PreviewId;
            
            await _db.OrganizationsStaging.AddRangeAsync(organizationStages);
            if (_db.SaveChanges() > 0)
            {
                await _db.UploadedFileses.AddAsync(uploaded);
                if (_db.SaveChanges() > 0)
                {
                    return Ok("Saved");
                }
            }

            return BadRequest("Operation failed!");
        }

        // PUT: api/Organizations/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

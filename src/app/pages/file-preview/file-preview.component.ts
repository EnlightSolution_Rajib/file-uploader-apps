import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrganizationService } from 'src/app/shared/_services/organization.service';
import { OrganizationStage } from 'src/app/shared/_model/organizationStage';

@Component({
  selector: 'app-file-preview',
  templateUrl: './file-preview.component.html',
  styleUrls: ['./file-preview.component.css']
})
export class FilePreviewComponent implements OnInit {

  previewId: string;
  organizations: OrganizationStage[];
  constructor(
    private routeValue: ActivatedRoute,
    private organizationService: OrganizationService
  ) { }

  ngOnInit() {
    this.previewId = this.routeValue.snapshot.params.previewId;
    this.getOrganizationByPreviewId();
  }

  getOrganizationByPreviewId() {
    this.organizationService.getOrganizationByPreviewID(this.previewId).subscribe((data: OrganizationStage[]) => {
      this.organizations = data;
    }, error => {
      console.log(error);
    });
  }

}

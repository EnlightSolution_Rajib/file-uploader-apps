import { Component, OnInit } from '@angular/core';
import { OrganizationStage } from 'src/app/shared/_model/organizationStage';
import { Organization } from 'src/app/shared/_model/organization';
import { OrganizationService } from 'src/app/shared/_services/organization.service';
import { NgxSpinnerService } from 'ngx-spinner';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Router } from '@angular/router';
import { FileUpload } from 'src/app/shared/_model/DTO/file-upload';
import { UploadFiles } from 'src/app/shared/_model/DTO/uploadedFilesDto';

@Component({
  selector: 'app-upload-organizations',
  templateUrl: './upload-organizations.component.html',
  styleUrls: ['./upload-organizations.component.css']
})
export class UploadOrganizationsComponent implements OnInit {
  isSideBar = true;
  previewId: string;
  isLoader = '';
  className = '';
  fileName: string;
  progress = 0;
  isUploaded = false;
  text: any;
  JSONData: Organization[] = new Array();
  fileReaded: Blob;
  title = 'read-excel-in-angular8';
  storeData: any;
  csvData: any;
  jsonData: any;
  textData: any;
  htmlData: any;
  fileUploaded: File;
  worksheet: any;
  organizationStages: OrganizationStage[] = new Array();
  uploadFiles: FileUpload = new FileUpload();
  uploadedFiles: UploadFiles[];
  constructor(
    private organizationService: OrganizationService,
    private spinner: NgxSpinnerService,
    private route: Router
  ) { }

  ngOnInit() {
    this.className = '';
    this.getAllUploadFiles();
  }
  getAllUploadFiles() {
    this.organizationService.getAllUploadFiles().subscribe((data: UploadFiles[]) => {
      this.uploadedFiles = data;
      console.log(this.uploadedFiles);
    });
  }
  toggleLti() {
    if (this.className === '') {
      this.className = 'active';
    } else {
      this.className = '';
    }
  }
  uploadedFile(event) {
    this.fileName = event.target.files[0].name;
    this.progress = 0;
    this.isUploaded = false;
    this.fileUploaded = event.target.files[0];
    this.readExcel();
  }
  readExcel() {
    let readFile = new FileReader();
    readFile.onload = (e) => {
      this.storeData = readFile.result;
      var data = new Uint8Array(this.storeData);
      var arr = new Array();
      for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
      var bstr = arr.join("");

      var workbook = XLSX.read(bstr, { type: 'binary' });
      var first_sheet_name = workbook.SheetNames[0];
      debugger
      this.worksheet = workbook.Sheets[first_sheet_name];
    };
    readFile.readAsArrayBuffer(this.fileUploaded);
  }

  uploadFile() {
    this.isSideBar = true;
    this.isLoader = 'loader';
    // this.readExcel();
    if (this.worksheet) {
      this.progress = 1;
      // progress
      let progress = setInterval(() => {
        if (this.progress <= 95) {
          if (this.isUploaded === false) {
            let randomValue = Math.floor(Math.random() * 5);
            this.progress += randomValue;
          }
          if (this.isUploaded === true) {
            this.progress = 100;
            clearInterval(progress);
            this.isLoader = '';
            // this.progress = 0;
          }
        }
        if (this.isUploaded === true) {
          this.progress = 100;
          clearInterval(progress);
          this.isLoader = '';
          // this.progress = 0;

        }
      }, 2000);
      // convert and upload files
      setTimeout(() => {
        console.log(this.worksheet);
        this.jsonData = XLSX.utils.sheet_to_json(this.worksheet, { raw: false });
        // db processing
        this.uploadFiles.previewId = this.generatePreviewId(8);
        this.previewId = this.uploadFiles.previewId;
        this.uploadFiles.fileName = this.fileName;

        this.jsonData.forEach(element => {
          let obj = new OrganizationStage();
          obj.organizationName = element.organizationName;
          obj.address = element.address;
          obj.previewId = this.previewId;
          this.uploadFiles.organizationStages.push(obj);
        });
        debugger
        this.organizationService.saveOrganization(this.uploadFiles).subscribe(data => {
          debugger
          if (data === 'Saved') {
            this.isUploaded = true;
          }
        }, error => {
          console.log(error);
        });
      });
    }


    console.log('Upload running...');
  }
  close() {
    this.progress = 0;
    // this.fileName = '';
    // let file = (<HTMLInputElement>document.getElementById('file')).files;
    // console.log(file);
    // debugger;
  }
  generatePreviewId(length) {
    let result = '';
    let preWord = 'Org' + new Date().getFullYear() + '-';
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return preWord + result;
  }

  showPreview(previewId) {
    if (previewId !== null && previewId !== '') {
      this.route.navigate(['/file-preview', previewId]);

    }
  }
  // readAsCSV() {
  //   this.csvData = XLSX.utils.sheet_to_csv(this.worksheet);
  //   const data: Blob = new Blob([this.csvData], { type: 'text/csv;charset=utf-8;' });
  //   FileSaver.saveAs(data, 'CSVFile' + new Date().getTime() + '.csv');
  // }
  // readAsJson() {
  //   console.log(this.worksheet);

  //   this.jsonData = XLSX.utils.sheet_to_json(this.worksheet, { raw: false });
  //   this.jsonData = JSON.stringify(this.jsonData);
  //   const data: Blob = new Blob([this.jsonData], { type: 'application/json' });
  //   console.log(this.jsonData);
  //   FileSaver.saveAs(data, 'JsonFile' + new Date().getTime() + '.json');
  // }
  // readAsHTML() {
  //   this.htmlData = XLSX.utils.sheet_to_html(this.worksheet);
  //   const data: Blob = new Blob([this.htmlData], { type: 'text/html;charset=utf-8;' });
  //   FileSaver.saveAs(data, 'HtmlFile' + new Date().getTime() + '.html');
  // }
  // readAsText() {
  //   this.textData = XLSX.utils.sheet_to_txt(this.worksheet);
  //   const data: Blob = new Blob([this.textData], { type: 'text/plain;charset=utf-8;' });
  //   FileSaver.saveAs(data, 'TextFile' + new Date().getTime() + '.txt');
  // }
  // convertFile(csv: any) {

  //   this.fileReaded = csv.target.files[0];

  //   let reader: FileReader = new FileReader();
  //   reader.readAsText(this.fileReaded);

  //   reader.onload = (e) => {
  //     let csv: any = reader.result;
  //     let allTextLines = csv.split(/\r|\n|\r/);
  //     let headers = allTextLines[0].split(',');
  //     let lines = [];

  //     for (let i = 0; i < allTextLines.length; i++) {
  //       // split content based on comma
  //       let data = allTextLines[i].split(',');
  //       if (data.length === headers.length) {
  //         let tarr = [];
  //         for (let j = 0; j < headers.length; j++) {
  //           tarr.push(data[j]);
  //         }

  //         // log each row to see output
  //         console.log(tarr);
  //         lines.push(tarr);
  //       }
  //     }
  //     // all rows in the csv file
  //     console.log(">>>>>>>>>>>>>>>>>", lines);
  //
  //   }
  // }

  //   csvJSON(csvText) {
  //     var lines = csvText.split("\n");

  //     var result = [];

  //     var headers = lines[0].split(",");
  //     console.log(headers);
  //     for (var i = 1; i < lines.length-1; i++) {

  //         var obj = {};
  //         var currentline = lines[i].split(",");

  //         for (var j = 0; j < headers.length; j++) {
  //             obj[headers[j]] = currentline[j];
  //         }

  //         result.push(obj);

  //     }

  //     console.log(JSON.stringify(result)); //JSON
  //     this.JSONData = JSON.stringify(result);
  //  }

  //   convertFile(input) {

  //   const reader = new FileReader();
  //   reader.readAsText(input.files[0]);
  //   reader.onload = () => {
  //     let text = reader.result;
  //     this.text = text;
  //     console.log(text);
  //     this.csvJSON(text);
  //     console.log(this.csvJSON);
  //   };

  //  }
}

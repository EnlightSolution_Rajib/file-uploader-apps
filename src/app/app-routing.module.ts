import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FilePreviewComponent } from './pages/file-preview/file-preview.component';
import { UploadOrganizationsComponent } from './pages/upload-organizations/upload-organizations.component';

const routes: Routes = [
    { path: 'file-preview/:previewId', component: FilePreviewComponent },
    { path: 'upload-organization', component: UploadOrganizationsComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }
  
export class Organization {
    public organizationName: string;
    public address: string;
    public previewId: string;
}

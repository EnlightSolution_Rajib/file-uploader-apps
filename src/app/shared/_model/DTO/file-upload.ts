import { OrganizationStage } from '../organizationStage';

export class FileUpload {
    public fileName: string;
    public previewId: string;
    public organizationStages: OrganizationStage[] = new Array();
}
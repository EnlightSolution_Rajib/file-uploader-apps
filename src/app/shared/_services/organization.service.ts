import { Organization } from './../_model/organization';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OrganizationStage } from '../_model/organizationStage';
import { FileUpload } from '../_model/DTO/file-upload';

const httpOptions = {
  headers: new HttpHeaders(
    {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
  )
};
@Injectable({
  providedIn: 'root'
})
export class OrganizationService {
  organizationAPI = ' http://localhost:5005/api/';
  constructor(private http: HttpClient) { }
  saveOrganization(uploadFiles: FileUpload) {
    debugger
    return this.http.post(this.organizationAPI + 'OrganizationsStaging/', uploadFiles, { headers: httpOptions.headers });
  }
  getOrganizationByPreviewID(previewID) {
    return this.http.get(this.organizationAPI + 'OrganizationsStaging/' + previewID);
  }
  getAllUploadFiles() {
    return this.http.get(this.organizationAPI + 'UploadFiles');
  }
}

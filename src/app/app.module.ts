import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { NgProgressModule } from 'ngx-progressbar';
import { HttpModule } from '@angular/http';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FilePreviewComponent } from './pages/file-preview/file-preview.component';
import { UploadOrganizationsComponent } from './pages/upload-organizations/upload-organizations.component'; 
import { AppRoutingModule } from './app-routing.module';
@NgModule({
  declarations: [
    AppComponent,
    FilePreviewComponent,
    UploadOrganizationsComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgProgressModule,
    HttpModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    AppRoutingModule
  ],
  providers: [
    // {provide: BrowserXhr, useClass: NgProgressBrowserXhr}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

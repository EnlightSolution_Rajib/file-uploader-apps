import { OrganizationService } from './shared/_services/organization.service';
import { Component, ElementRef, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import { debug } from 'util';
import { Organization } from './shared/_model/organization';
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { OrganizationStage } from './shared/_model/organizationStage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    constructor() { }
  ngOnInit(): void {
    
  }
  
}
